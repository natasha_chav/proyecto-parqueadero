    /*Transicion*/
    const $btnSignIn= document.querySelector('.sign-in-btn'),
    $btnSignUp= document.querySelector('.sign-up-btn'),
    $signUp=document.querySelector('.sign-up'),
    $signIn=document.querySelector('.sign-in');
    
    document.addEventListener('click',e=>{
        if(e.target=== $btnSignIn || e.target === $btnSignUp){
            $signIn.classList.toggle('active');
            $signUp.classList.toggle('active')
        }
    });
    
    /*Registro*/
    
    function Registro() {

        var nombre = document.getElementById("nombre").value;
        var cedula = document.getElementById("cedula").value;
        var celular = document.getElementById("celular").value;
        var correo = document.getElementById("correo").value;
        var contraseña = document.getElementById("contraseña").value;
     
    
        if (nombre == "" || cedula == "" || celular =="" || correo == "" || contraseña == "") {
          alert("Por favor, complete todos los campos del formulario.");
          return false;
        } 
     
    
        var correoRegex = /\S+@\S+\.\S+/;
        if (!correoRegex.test(correo)) {
          alert("Por favor, ingrese un correo electrónico válido.");
          return false;
        }
    
      
        alert("Usuario registrado con éxito");
        return true;
    }
    
    
    /*Login*/
    function Inicio(){
       
      var email=document.getElementById("email").value;
      var contraseña=document.getElementById("contra").value;
    
      if(email==""& contraseña==""){
        alert("Por favor ingrese su correo y contraseña")
      }
       else{
        window.location="index.html";
       }
    }
      
    